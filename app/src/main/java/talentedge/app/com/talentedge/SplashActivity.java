package talentedge.app.com.talentedge;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import talentedge.app.com.talentedge.Constant.AppConstant;

/**
 * Created by ${ShalviSharma} on 11/20/15.
 */
public class SplashActivity extends Activity {
    private int SPLASH_TIME_OUT = 1000;
    SharedPreferences sharedPreferences;
    boolean hasvisited;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getSharedPreferences(AppConstant.TALENTEDGE,
                Context.MODE_PRIVATE);
        hasvisited = sharedPreferences.getBoolean("Visited", false);
/* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                if (hasvisited) {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                }

            }
        }, SPLASH_TIME_OUT);

    }
}
