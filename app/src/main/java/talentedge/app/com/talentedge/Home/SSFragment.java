package talentedge.app.com.talentedge.Home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import talentedge.app.com.talentedge.R;

/**
 * Created by ${ShalviSharma} on 11/20/15.
 */
public class SSFragment extends Fragment {
    View rootview;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_ss,container,false);

        return rootview;
    }

}
