package talentedge.app.com.talentedge;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import talentedge.app.com.talentedge.Constant.AppConstant;


public class MainActivity extends Activity {
    ViewPager _mViewPager;
    ArrayList<Integer> mResources;
    CirclePageIndicator mIndicator;
    private MainPagerAdapter _adapter;
    LayoutInflater infl;
    ImageView imageView;
    Button mButton_back;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedpreferences = getSharedPreferences(AppConstant.TALENTEDGE,
                Context.MODE_PRIVATE);

        mResources = new ArrayList<Integer>();
        mResources.add(R.drawable.bg_1);
        mResources.add(R.drawable.bg_2);
        mResources.add(R.drawable.bg_3);
        mResources.add(R.drawable.bg_4);
        _adapter = new MainPagerAdapter();
        _mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);

        infl = (LayoutInflater) getApplicationContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < mResources.size(); i++) {
            RelativeLayout v1 = (RelativeLayout) infl.inflate(R.layout.custom_image,
                    null);
            imageView = (ImageView) v1.findViewById(R.id._custom);
            mButton_back = (Button) v1.findViewById(R.id.button_skip);
            imageView.setImageResource(mResources.get(i));


            if (i == 3) {

                mButton_back.setVisibility(View.VISIBLE);
                Log.e("i----", "" + i);
                mButton_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("here", "clicked");
                        shared_preferences();
                        Intent iIntent = new Intent(MainActivity.this, LoginActivity.class);

                        startActivity(iIntent);
                        finish();
                    }
                });

            }
            addView(v1);

        }
        _mViewPager.setAdapter(_adapter);
        mIndicator.setViewPager(_mViewPager);
    }


    private void addView(View newPage) {

        int pageIndex = _adapter.addView(newPage);
        _mViewPager.setCurrentItem(pageIndex, true);
    }


    private void shared_preferences() {
        editor = sharedpreferences.edit();
        editor.putBoolean("Visited", true);
        editor.commit();
    }

}
