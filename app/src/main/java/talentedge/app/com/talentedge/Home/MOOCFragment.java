package talentedge.app.com.talentedge.Home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import talentedge.app.com.talentedge.MOOC.ITFragment;
import talentedge.app.com.talentedge.R;

/**
 * Created by ${ShalviSharma} on 11/20/15.
 */
public class MOOCFragment extends Fragment {
    View rootview;
    Button btnIt,btnFinance,btnHr,btnstartUp,btnops,btnProduct;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_mooc, container, false);
        btnIt = (Button) rootview.findViewById(R.id.btn_it);
        btnFinance = (Button) rootview.findViewById(R.id.btn_finance);
        btnHr = (Button) rootview.findViewById(R.id.btn_hr);
        btnstartUp = (Button) rootview.findViewById(R.id.btn_startup);
        btnops = (Button) rootview.findViewById(R.id.btn_business);
        btnProduct = (Button) rootview.findViewById(R.id.btn_product);
        btnIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ITFragment detailFragment = new ITFragment();
                fragmentTransaction.replace(android.R.id.content, detailFragment).addToBackStack(null);

                fragmentTransaction.commit();
            }
        });
        btnFinance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ITFragment detailFragment = new ITFragment();
                fragmentTransaction.replace(android.R.id.content, detailFragment).addToBackStack(null);

                fragmentTransaction.commit();
            }
        });
        btnHr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ITFragment detailFragment = new ITFragment();
                fragmentTransaction.replace(android.R.id.content, detailFragment).addToBackStack(null);

                fragmentTransaction.commit();
            }
        });
        btnstartUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ITFragment detailFragment = new ITFragment();
                fragmentTransaction.replace(android.R.id.content, detailFragment).addToBackStack(null);

                fragmentTransaction.commit();
            }
        });
        btnops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ITFragment detailFragment = new ITFragment();
                fragmentTransaction.replace(android.R.id.content, detailFragment).addToBackStack(null);

                fragmentTransaction.commit();
            }
        });
        btnProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ITFragment detailFragment = new ITFragment();
                fragmentTransaction.replace(android.R.id.content, detailFragment).addToBackStack(null);

                fragmentTransaction.commit();
            }
        });

        return rootview;
    }

}
