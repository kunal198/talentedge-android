package talentedge.app.com.talentedge.Home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import talentedge.app.com.talentedge.R;
import talentedge.app.com.talentedge.Settings.ProfileActivity;

/**
 * Created by ${ShalviSharma} on 11/20/15.
 */
public class SettingsFragment extends Fragment {
    View rootview;
    RelativeLayout relativeLayoutProfile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_settings,container,false);
        relativeLayoutProfile =(RelativeLayout)rootview.findViewById(R.id.relative_details);
        relativeLayoutProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i_profile =  new Intent(getActivity(), ProfileActivity.class);
                startActivity(i_profile);

            }
        });

        return rootview;
    }
}
