package talentedge.app.com.talentedge.Home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import talentedge.app.com.talentedge.R;

/**
 * Created by ${ShalviSharma} on 11/20/15.
 */
public class A3Fragment extends Fragment {
    View rootview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_a3,container,false);
        return rootview;
    }
}
