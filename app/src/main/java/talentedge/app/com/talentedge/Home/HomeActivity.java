package talentedge.app.com.talentedge.Home;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.util.Log;

import talentedge.app.com.talentedge.AppController;
import talentedge.app.com.talentedge.R;

;

/**
 * Created by ${ShalviSharma} on 11/20/15.
 */
public class HomeActivity extends AppCompatActivity {
    static final int NUM_ITEMS = 4;

    ViewPager mPager;
    SlidePagerAdapter mPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

		/* Instantiate a ViewPager and a PagerAdapter. */
        mPager = (ViewPager) findViewById(R.id.viewPager);
        mPagerAdapter = new SlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

    }

    public static class SlidePagerAdapter extends FragmentStatePagerAdapter {
        public static final String ARG_OBJECT = "object";
        String[] title = {
                "SS",
                "A3",
                "MOOC",
                "SETTINGS"};
        private Drawable a,b;


        public SlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0:
                    SSFragment fragment = new SSFragment();
                    return fragment;

                case 1:
                    A3Fragment fragment1 = new A3Fragment();
                    return fragment1;
                case 2:
                    MOOCFragment fragment2 = new MOOCFragment();
                    return fragment2;
                case 3:
                    SettingsFragment fragment3 = new SettingsFragment();
                    return fragment3;

            }
            return null;
        }

        @Override
        public int getCount() {
            // For this contrived example, we have a 100-object collection.
            return NUM_ITEMS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            SpannableStringBuilder sb = new SpannableStringBuilder("  " + ("")); // space added before text for convenience
            Context context = AppController.context;
            Log.e("position---",""+position);


    /*        if (position==0){
                a = context.getResources().getDrawable( R.drawable.ic_ss_selected);
                a.setBounds(0, 0, a.getIntrinsicWidth(), a.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(a, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            else {
                a = context.getResources().getDrawable( R.drawable.ic_ss_unselected);
                a.setBounds(0, 0, a.getIntrinsicWidth(), a.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(a, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            if (position==1){
                a = context.getResources().getDrawable( R.drawable.ic_ss_selected);
                a.setBounds(0, 0, a.getIntrinsicWidth(), a.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(a, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            else {
                a = context.getResources().getDrawable( R.drawable.ic_ss_unselected);
                a.setBounds(0, 0, a.getIntrinsicWidth(), a.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(a, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            if (position==2){
                a = context.getResources().getDrawable( R.drawable.ic_ss_selected);
                a.setBounds(0, 0, a.getIntrinsicWidth(), a.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(a, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            else {
                a = context.getResources().getDrawable( R.drawable.ic_ss_unselected);
                a.setBounds(0, 0, a.getIntrinsicWidth(), a.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(a, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            if (position==3){
                a = context.getResources().getDrawable( R.drawable.ic_ss_selected);
                a.setBounds(0, 0, a.getIntrinsicWidth(), a.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(a, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            else {
                a = context.getResources().getDrawable( R.drawable.ic_ss_unselected);
                a.setBounds(0, 0, a.getIntrinsicWidth(), a.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(a, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }*/

            return sb;

        }






    }








}
