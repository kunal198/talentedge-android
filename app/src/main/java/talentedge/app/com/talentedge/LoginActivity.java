package talentedge.app.com.talentedge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import talentedge.app.com.talentedge.Home.HomeActivity;

/**
 * Created by ${ShalviSharma} on 11/19/15.
 */
public class LoginActivity extends Activity {
    TextView txtRegister;
    LinearLayout linearLayoutLinkedin,linearLayoutEmail;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtRegister=(TextView)findViewById(R.id.txt_new_user);
        linearLayoutLinkedin =(LinearLayout)findViewById(R.id.linear_linkedin);
        linearLayoutEmail =(LinearLayout)findViewById(R.id.linear_email);



        linearLayoutLinkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister =  new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intentRegister);
                finish();
            }
        });


        linearLayoutEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister =  new Intent(LoginActivity.this,HomeActivity.class);
                startActivity(intentRegister);
                finish();
            }
        });






        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister =  new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intentRegister);
            }
        });





    }
}
