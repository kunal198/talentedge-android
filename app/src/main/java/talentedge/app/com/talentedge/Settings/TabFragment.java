package talentedge.app.com.talentedge.Settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import talentedge.app.com.talentedge.R;

/**
 * Created by ${ShalviSharma} on 11/23/15.
 */
public class TabFragment extends Fragment {
    private ListAdapter mAdapter;
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;

    private TextView tabText;
    public static Fragment newInstance() {
        return new TabFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab, container, false);



        return view;
    }


}
